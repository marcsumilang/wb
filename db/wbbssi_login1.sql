-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2019 at 12:56 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wbbssi_login1`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_account`
--

CREATE TABLE `customer_account` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `age` varchar(30) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `municipality` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `picture` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_account`
--

INSERT INTO `customer_account` (`id`, `firstname`, `middlename`, `lastname`, `age`, `gender`, `contact`, `municipality`, `email`, `password`, `picture`) VALUES
(1, 'ronald', 'fortes', 'padaoan', '24', 'male', '09884747432', 'bulacan', 'ronald@gmail.com', '1234', 'ermancivici.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `customer_pending`
--

CREATE TABLE `customer_pending` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `age` varchar(30) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `municipality` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL,
  `picture` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investor_account`
--

CREATE TABLE `investor_account` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `age` varchar(30) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `municipality` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL,
  `picture` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `investor_account`
--

INSERT INTO `investor_account` (`id`, `firstname`, `middlename`, `lastname`, `age`, `gender`, `contact`, `municipality`, `email`, `password`, `picture`) VALUES
(1, 'arjun', 'c', 'de luna', '18', 'male', '09067754084', '4', 'arjun@gmail.com', '1234', '1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `investor_pending`
--

CREATE TABLE `investor_pending` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `age` varchar(30) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `municipality` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL,
  `picture` varchar(250) NOT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lobby`
--

CREATE TABLE `lobby` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `image` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `municipality`
--

CREATE TABLE `municipality` (
  `municipality_id` int(11) NOT NULL,
  `municipality_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `municipality`
--

INSERT INTO `municipality` (`municipality_id`, `municipality_name`) VALUES
(1, 'Angat'),
(2, 'Balagtas'),
(3, 'Baliuag'),
(4, 'Bocaue'),
(5, 'Bulacan,Bulacan'),
(6, 'Bustos'),
(7, 'Calumpit'),
(8, 'Dona Remedios Trinidad'),
(9, 'Guiguinto'),
(10, 'Hagonoy'),
(11, 'City of Malolos'),
(12, 'Marilao'),
(13, 'Norzagaray'),
(14, 'Obando'),
(15, 'Pandi'),
(16, 'Paombong'),
(17, 'Plaridel'),
(18, 'San Ildefonso'),
(19, 'City of San Jose del Monte'),
(20, 'San Miguel'),
(21, 'San Rafael'),
(22, 'Santa Maria'),
(23, 'Pulilan'),
(24, 'Meycauyan');

-- --------------------------------------------------------

--
-- Table structure for table `pending`
--

CREATE TABLE `pending` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `age` varchar(100) NOT NULL,
  `emailaddress` varchar(100) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `municipality_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pending`
--

INSERT INTO `pending` (`id`, `firstname`, `middlename`, `lastname`, `gender`, `age`, `emailaddress`, `contact`, `province`, `municipality_id`, `username`, `password`, `type`) VALUES
(1, '1', '1', '1', '1', '1', '1', '1', '1', 1, 'admin', 'admin', 'admin'),
(2, '2', '2', '2', '2', '2', '2', '2', '2', 2, 'client', 'client', 'Customer'),
(3, '3', '3', '3', '3', '3', '3', '3', '3', 3, 'investor', 'investor', 'Investor'),
(5, '5', '5', '5', '5', '5', 'bautista04job@gmail.com', '5', '5', 5, 'Investorsample', 'Investorsample', 'Investor'),
(6, '1', '1', '1', '1', '1', '1', '1', '1', 15, '123', '123', 'Customer'),
(7, '2', '2', '2', '2', '2', '2', '2', '2', 15, '2', '2', 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `age` varchar(100) NOT NULL,
  `emailaddress` varchar(100) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `municipality_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `validation` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `firstname`, `middlename`, `lastname`, `gender`, `age`, `emailaddress`, `contact`, `province`, `municipality_id`, `username`, `password`, `type`, `validation`) VALUES
(51, 'Kk', 'Kk', 'Kk', '', '0', 'Kk', '0998776', 'Bulacan', 1, 'Kk', 'KK', 'Investor', 0),
(52, 'ronald', 'fortes', 'padaoan', '', '0', 'ronaldpadaoan@gmail.com', '09656100973', 'bulacan', 1, 'ronald', 'ronald', 'Investor', 0),
(53, 'jomarie', 'valencia', 'ajose', '', 'Male', 'zxc', '2111215452', 'balagtas', 2, 'waldo', 'waldo', 'Investor', 0),
(54, 'jomarie', '', 'ajose', '', '19-30', 'askdoaksdo', '123', 'bulacan', 2, 'waldo', 'waldo', 'Investor', 0),
(55, 'jomarie', 'valencia', 'ajose', 'Female', '18 Below', 'jomasdjaosdjo', '129312381238', 'balagtas', 2, 'gelo', 'gelo', 'Customer', 0),
(56, 'ronald', 'fortes', 'padaoan', 'Male', '19-30', 'ronaldpadaoan@gmail.com', '09656100973', 'bulacan', 3, 'qwerty', 'qwerty', 'Investor', 0),
(57, 'job', 'job', 'job', 'Male', '18 Below', '111', '1', 'bulacan', 3, 'job', 'job', 'Customer', 0);

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE `survey` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `gadgets` varchar(255) NOT NULL,
  `apparrels` varchar(255) NOT NULL,
  `accessories` varchar(255) NOT NULL,
  `equipments` varchar(255) NOT NULL,
  `cosmetics` varchar(255) NOT NULL,
  `grocery` varchar(255) NOT NULL,
  `for_family` varchar(255) NOT NULL,
  `horror` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `romance` varchar(255) NOT NULL,
  `drama` varchar(255) NOT NULL,
  `comedy` varchar(255) NOT NULL,
  `fast_food` varchar(255) NOT NULL,
  `buffet` varchar(255) NOT NULL,
  `fine_dining` varchar(255) NOT NULL,
  `kiosk` varchar(255) NOT NULL,
  `playing_arcade` varchar(255) NOT NULL,
  `singing` varchar(255) NOT NULL,
  `municipality` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`id`, `id_customer`, `gadgets`, `apparrels`, `accessories`, `equipments`, `cosmetics`, `grocery`, `for_family`, `horror`, `action`, `romance`, `drama`, `comedy`, `fast_food`, `buffet`, `fine_dining`, `kiosk`, `playing_arcade`, `singing`, `municipality`) VALUES
(70, 2, '7', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 2),
(71, 3, '4', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 3),
(107, 12, '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 5);

-- --------------------------------------------------------

--
-- Table structure for table `toshop`
--

CREATE TABLE `toshop` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_municipality` int(11) NOT NULL,
  `gadgets` text NOT NULL,
  `apparrels` text NOT NULL,
  `equipments` text NOT NULL,
  `accessories` text NOT NULL,
  `grocery` text NOT NULL,
  `cosmetics` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toshop`
--

INSERT INTO `toshop` (`id`, `id_customer`, `id_municipality`, `gadgets`, `apparrels`, `equipments`, `accessories`, `grocery`, `cosmetics`) VALUES
(1, 1, 1, '1', '1', '1', '1', '1', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_account`
--
ALTER TABLE `customer_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_pending`
--
ALTER TABLE `customer_pending`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investor_account`
--
ALTER TABLE `investor_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investor_pending`
--
ALTER TABLE `investor_pending`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `lobby`
--
ALTER TABLE `lobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `municipality`
--
ALTER TABLE `municipality`
  ADD PRIMARY KEY (`municipality_id`);

--
-- Indexes for table `pending`
--
ALTER TABLE `pending`
  ADD PRIMARY KEY (`id`),
  ADD KEY `municipality_id` (`municipality_id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`),
  ADD KEY `municipality_id` (`municipality_id`);

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toshop`
--
ALTER TABLE `toshop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_customer_2` (`id_customer`),
  ADD KEY `id_municipality` (`id_municipality`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_account`
--
ALTER TABLE `customer_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer_pending`
--
ALTER TABLE `customer_pending`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `investor_account`
--
ALTER TABLE `investor_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `investor_pending`
--
ALTER TABLE `investor_pending`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lobby`
--
ALTER TABLE `lobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `municipality`
--
ALTER TABLE `municipality`
  MODIFY `municipality_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pending`
--
ALTER TABLE `pending`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `toshop`
--
ALTER TABLE `toshop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pending`
--
ALTER TABLE `pending`
  ADD CONSTRAINT `pending_ibfk_1` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`municipality_id`);

--
-- Constraints for table `register`
--
ALTER TABLE `register`
  ADD CONSTRAINT `register_ibfk_1` FOREIGN KEY (`municipality_id`) REFERENCES `municipality` (`municipality_id`);

--
-- Constraints for table `survey`
--
ALTER TABLE `survey`
  ADD CONSTRAINT `survey_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `pending` (`id`),
  ADD CONSTRAINT `survey_ibfk_2` FOREIGN KEY (`municipality`) REFERENCES `municipality` (`municipality_id`);

--
-- Constraints for table `toshop`
--
ALTER TABLE `toshop`
  ADD CONSTRAINT `toshop_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `pending` (`id`),
  ADD CONSTRAINT `toshop_ibfk_2` FOREIGN KEY (`id_municipality`) REFERENCES `municipality` (`municipality_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
