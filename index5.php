<?php
include ('main_connection.php');
$query = "SELECT sum(gadgets),sum(apparrels),sum(accessories),sum(equipments),sum(cosmetics),sum(grocery),municipality.municipality_name FROM survey inner join municipality on survey.municipality = municipality.municipality GROUP by municipality.municipality_name";
$result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="admin.php">Online Survey For Investor</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="radmin.php">Register<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="add.php">Add Trivia</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="ud.php">Update/Delete</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Statistics
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index4.php">To Shop</a>
          <a class="dropdown-item" href="index5.php">To Watch</a>
          <a class="dropdown-item" href="index6.php">To Eat</a>
          <a class="dropdown-item" href="index7.php">To Have Fun</a>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pending
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">

          <a class="dropdown-item" href="table.php">Investor Request</a>
          <a class="dropdown-item" href="table.php">Survey Request</a>
      </li>
      <li><a href="logout.php"  onclick="return confirm('Are you sure you want to Log-out')">Logout</a></li> 	 
    </ul>
    
  </div>
</nav>
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Statistics</title>
		<script src="jquery.min.js"></script>
		<link rel="stylesheet" href="bootstrap.min.css" />
		<link rel="stylesheet" href="jquery-ui.css">
		<script src="bootstrap.min.js"></script>
		<script src="jquery.highchartTable.js"></script>
		<script src="highcharts.js"></script>
		<script src="jquery-ui.js"></script>
	</head>
	<body>
		<br />
		<div class="container">
			<h3 align="center">What do you often do when you go to the Mall?</h3>
			<h3 align="center">To Watch</h3>
			<br />
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="for_chart">
					<thead>
						<tr>
						
							<th width="14%">Municipality</th>
							<th width="14%">For Family</th>
							<th width="14%">Horror</th>
							<th width="14%">Action</th>
							<th width="14%">Romance</th>
							<th width="14%">Drama</th>
							<th width="14%">Comedy</th>
							
							
						</tr>
					</thead>
					<?php
					while($row = mysqli_fetch_array($result))
					{
						echo '
						<tr>
						
							<td>'.$row[6].'</td>
							<td>'.$row[0].'</td>
							<td>'.$row[1].'</td>
							<td>'.$row[2].'</td
							<td>'.$row[3].'</td>
							<td>'.$row[4].'</td>
							<td>'.$row[5].'</td>
							<td>'.$row[5].'</td>
							
						</tr>
						';
					}
					?>
				</table>
			</div>
			<br />
			<div id="chart_area" title="Answer Survey Chart">
				
			</div>
			<br />
			<div align="center">
				<button type="button" name="view_chart" id="view_chart" class="btn btn-info btn-lg">View Data in Chart</button>
			</div>
			<br />
			<br />
		</div>
	</body>
</html>

<script>
$(document).ready(function(){
		
	$('#view_chart').click(function(){
		$('#for_chart').data('graph-container', '#chart_area');
		$('#for_chart').data('graph-type', 'column');
		$("#chart_area").dialog('open');
		$('#for_chart').highchartTable();
		
	
	});
	

	
	$("#chart_area").dialog({
		autoOpen:false,
		width: 1000,
		height:700,
	});
});
</script>



