<?php
//connection
session_start();
  
$host='wbbssi.com';//default
$username='wbbssi';//default by localhost
$password='1nut3l1ns1d3';
$database='wbbssi_login1'; //db name


$con = mysqli_connect($host,$username,$password,$database);
  if(isset($_POST['login'])){
    $username = mysqli_real_escape_string($con,$_POST['username']);
    $password = mysqli_real_escape_string($con,$_POST['password']);

    $sql = "SELECT * FROM pending WHERE username = ? AND password = ?";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("ss", $username,$password);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();

    session_regenerate_id();
    $_SESSION['id'] = $row['id'];
    $_SESSION['municipality'] =$row['municipality_id'];
    $_SESSION['username'] = $row['username'];
    $_SESSION['password'] = $row['password'];
    $_SESSION['type'] = $row['type'];
    $_SESSION['validation'] = $row['validity'];
    session_write_close();


    if($result->num_rows == 1 && $_SESSION['type'] == "admin" && $_SESSION['username'] ){
        header("location:admin.php");
    }else if($result->num_rows == 1 && $_SESSION['type'] == "Investor" && $_SESSION['username']){
            header("location:index2.php");
    }else if($result->num_rows ==1 && $_SESSION['type'] == "Client" && $_SESSION['username']){
            echo "<script>alert('VALIDATED');</script>";
    }else if($result->num_rows ==1 && $_SESSION['type'] == "Customer" && $_SESSION['username']){
        header("location:survey2.php?id=".$row['id']."&municipality=".$row['municipality_id']);
            echo "<script>alert('VALIDATED');</script>";
    }
    else{
      echo "<script>alert('Username or Password is incorrect!!'); </script>";  
    }
  }
?>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="icon" type="images" href="images/bpclogo.png">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
      <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Survey For Online Investors</a>
    </div>
    <ul class="nav navbar-nav">
    </ul>
    <form class="navbar-form navbar-left" action="/action_page.php">
      <div class="form-group">
        
    </form>

  </div>
</nav>
<br>
<br>
<center><div style="width: 400px; border-style: solid;padding: 25px;">

  <center><h3> Login Here</h3></center>
<hr>
<form action="login.php" method="POST" >
  
<b>Username</b>
<input type="text"name="username" style="width: 100%;" required>

<b>Password</b>
<input type="password" name="password" style="width: 100%;" required>
<br>
<br>
   <center> <button type="submit" name="login" class="submitbtn">Login</button> 
</form>
<br>
<br>
<p>Don't Have an Account ? <a href="register.php"> Register Here !!</a></p>
</div></center>
</body>
</html>