<?php 
include('../main_connection.php');
if(isset($_GET['id'])){
  $id= $_GET['id'];
  $sql = "SELECT * FROM `survey_questions` where `id` = '$id'";
  $result = mysqli_query($conn,$sql);
  while($row = mysqli_fetch_array($result)){
    $category = $row[1];
    $question = $row[2];
    $choices = $row[3];
  }
  $update = true;
}
$category= "--Select Category--";
$question= "";
$choices= "";
$update = false;

$sql = "SELECT * FROM `survey_categories`";
$survey_categories = mysqli_query($conn, $sql);

 if (isset($_POST['submit'])) {   
  $category = $_POST['category'];     
  $question = $_POST['question'];     
  $choices = $_POST['choices'];  
  $investor_name = $_SESSION['investor_name'];
     
    if(isset($_POST['update']) && $_POST['update'] != ""){
        $id = $_POST['id'];
        $query = "UPDATE `survey_questions` SET `category`='$category', `question`='$question', `choices`='$choices', `submitted_by`='$investor_name' , `status`='APPROVED' WHERE  `id`='$id'"; 
        $result = mysqli_query($conn,$query);
    }else{
      $query = "INSERT INTO `survey_questions` (`id`, `category`, `question`, `choices`, `submitted_by`, `answers`, `status`) VALUES (null, '$category', '$question', '$choices', '$investor_name', '0', 'APPROVED')"; 
      $result = mysqli_query($conn,$query);
    }          
    if($result){  
      $name= "";
      echo "
        <script>
            alert('Successful')
        </script>
      ";
      exit();
      } else {
        echo "
          <script>
              alert('Failed')
          </script>
          ";   
        }

}
    ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Wbbssi - Online Survey for Investors</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<!-- Favicons -->
<!-- <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml"> -->
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
  <body>
  <?php
  include('nav.php');
  ?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
      <h2>Survey</h2> 
      </div>     
      <form action="submit-survey.php" method="POST" style="width: 60%; padding:10px;" >
        <b>Category</b>
        <input type="text" name="update" value="<?php echo $update; ?>" hidden>
        <input type="text" name="id" value="<?php echo $id; ?>" hidden>
        <select name="category" class="form-control" required>
        <option value='<?php echo $category; ?>' selected><?php echo $category; ?></option>
        <?php        
          while($row = mysqli_fetch_array($survey_categories)){
            echo"            
              <option value='$row[1]'>$row[1]</option>
            ";
          }
        ?>
        </select>        
        <b>Question</b>
        <input type="text" placeholder=""  class="form-control" name="question" value="<?php echo $question; ?>"   style="width: 100%;"  required>        
        <b>Choices (Note: seperate choices with comma ',' eg. "choice1, choice2")</b> 
        <Textarea name="choices"  class="form-control" > <?php echo $choices; ?></Textarea>
        <br>
        <?php
          if($update == true){
            echo "<input type='submit' class='btn btn-primary' name='submit' value='update' > ";
          }else{
            echo "<input type='submit' class='btn btn-primary' name='submit' value='Add' > ";
          }
        ?>
          
      </form>
    </main>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script src="dashboard.js"></script>
</body>
</html>
