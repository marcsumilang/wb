<?php  
include('main_connection.php');

$query = "SELECT SUM(gadgets) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$gadgets = $row['value_sum'];

$query = "SELECT SUM(apparrels) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$apparrels = $row['value_sum'];

$query = "SELECT SUM(accessories) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$accessories = $row['value_sum'];

$query = "SELECT SUM(equipments) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$equipments = $row['value_sum'];

$query = "SELECT SUM(cosmetics) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$cosmetics = $row['value_sum'];

$query = "SELECT SUM(grocery) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$grocery = $row['value_sum'];

$query = "SELECT SUM(horror) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$horror = $row['value_sum'];

$query = "SELECT SUM(for_family) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$for_family = $row['value_sum'];

$query = "SELECT SUM(action) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$action = $row['value_sum'];

$query = "SELECT SUM(romance) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$romance = $row['value_sum'];

$query = "SELECT SUM(drama) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$drama = $row['value_sum'];

$query = "SELECT SUM(comedy) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$comedy = $row['value_sum'];

$query = "SELECT SUM(fast_food) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$fast_food = $row['value_sum'];

$query = "SELECT SUM(buffet) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$buffet = $row['value_sum'];

$query = "SELECT SUM(fine_dining) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$fine_dining = $row['value_sum'];

$query = "SELECT SUM(kiosk) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$kiosk = $row['value_sum'];

$query = "SELECT SUM(playing_arcade) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$playing_arcade = $row['value_sum'];

$query = "SELECT SUM(singing) AS value_sum FROM survey";    
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result); 
$singing = $row['value_sum'];


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>POLL</title>
</head>
<body>
    SURVEY POLL
    <table class="table table-bordered table-striped"> 
        <tr>
            <th>gadgets</th>
            <th>apparrels</th>
            <th>accessories</th>
            <th>equipments</th>
            <th>cosmetics</th>
            <th>grocery</th>
            <th>for_family</th>
            <th>horror</th>
            <th>action</th>
            <th>romance</th>
            <th>drama</th>
            <th>comedy</th>
            <th>fast_food</th>
            <th>buffet</th>
            <th>fine_dining</th>
            <th>kiosk</th>
            <th>playing_arcade</th>
            <th>singing</th>
        </tr>
        <tr>
            <td><?php echo $gadgets; ?></td>
            <td><?php echo $apparrels; ?></td>
            <td><?php echo $accessories; ?></td>
            <td><?php echo $equipments; ?></td>
            <td><?php echo $cosmetics; ?></td>
            <td><?php echo $grocery; ?></td>
            <td><?php echo $for_family; ?></td>
            <td><?php echo $horror; ?></td>
            <td><?php echo $action; ?></td>
            <td><?php echo $romance; ?></td>
            <td><?php echo $drama; ?></td>
            <td><?php echo $comedy; ?></td>
            <td><?php echo $fast_food; ?></td>
            <td><?php echo $buffet; ?></td>
            <td><?php echo $fine_dining; ?></td>
            <td><?php echo $kiosk; ?></td>
            <td><?php echo $playing_arcade; ?></td>
            <td><?php echo $singing; ?></td>
        </tr>
    </table>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>