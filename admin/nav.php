
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">WBBSSI</a>
  <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="#">Sign out</a>
    </li>
  </ul>
</nav>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="dashboard.php">
              <span data-feather="home"></span>
              Dashboard <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="survey.php">
              <span data-feather="layers"></span>
              <!-- <span data-feather="file"></span> -->
              Survey
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="survey-categories.php">
              <span data-feather="layers"></span>
              <!-- <span data-feather="shopping-cart"></span> -->
              Survey-Categories
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="trivia.php">
              <span data-feather="layers"></span>
              <!-- <span data-feather="shopping-cart"></span> -->
              Trivia
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="administrators.php">
              <span data-feather="users"></span>
              Administrators
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="account-request.php">
              <span data-feather="users"></span>
              Account Request
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="investors.php">
              <span data-feather="users"></span>
              Investors
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="users.php">
              <span data-feather="users"></span>
              Users
            </a>
          </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Saved reports</span>
          <a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="bar-chart-2"></span>
              Statistics
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Current month
            </a>
          </li>
        </ul>
      </div>
    </nav>
