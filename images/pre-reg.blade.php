<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Centralized Database">
        <meta name="author" content="Bulacan Polytechnic College">
        <meta name="generator" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Bulacan Polytechnic College</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link href="swiper.min.css" type="text/css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{asset('css/pre-reg.css')}}" type="text/css" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <link rel="stylesheet" href="{{asset('css/app.css')}}"> --}}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://www.google.com/recaptcha/api.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    </head>
    <body>
            <div class="header">
                <div class="logo">
                        <a href="{{route('index')}}"><img src="{{asset('image/bpclogo.png')}}"/></a>
                </div>
                <!--shool name-->
                <h4 class="bpcname"> <a href="{{route('index')}}">Bulacan<br>Polytechnic College</a> </h4>
                <label for="chk" class="show-menu-btn">
                        <i class="fa fa-ellipsis-h"></i>
                </label>
                        <ul class="sub-menu">
                        <!--<a href="#"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;Home</a>-->
                        <a href="#"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;Contact Us</a>
                        <a href="#"><i class="fa fa-info" aria-hidden="true"></i>&nbsp;About Us</a>
                        <i class="fa fa-times"></i>
                        </label>
                </ul>
            </div>
            <form  {{route('store_pre_register')}} method="POST" id="form" onsubmit="return submit_form()">
            {{csrf_field()}}
            <div class="content">
                <div class="container">
                    <div class="page_title section">
                        <h3>Student Pre-registration Form</h3>
                    </div>    
                    <div class="academic_info section">
                        <span >* Required</span>
                        <div class="title">
                            <h4>Academic Information</h4>
                        </div>
                        <div class="type_of_student">
                            <div class="heading">
                                Type of Student
                            </div>
                            <input type="text" value="{{$type->type}}" readonly>
                            <input type="hidden" name="type_id" value="{{$type->id}}">
                        </div>
                        @foreach ($year_section as $yr_sec)
                            <div class="campus">
                                <div class="heading">
                                    Campus
                                </div>
                                <input type="text" value="{{$yr_sec->campus->campus_name}}" readonly>
                                <input type="hidden" name="campus_id" value="{{$yr_sec->campus_id}}">
                            </div>
                            <div class="school_year">
                                <div class="heading">
                                    School Year
                                </div>
                                <input type="text" value="{{$yr_sec->school_year->school_year}}" readonly>
                                <input type="hidden" name="school_year_id" value="{{$yr_sec->school_year_id}}">
                            </div>
                            <div class="semester">
                                <div class="heading">
                                    Semester
                                </div>
                                <input type="text" value="{{$yr_sec->term->term}}" readonly>
                                <input type="hidden" name="sem_id" value="{{$yr_sec->term_id}}">
                            </div>
                            <div class="year_section">
                                <div class="heading">
                                    Year and Section
                                </div>
                                <input type="text" value="{{$yr_sec->course->course_code}} {{$yr_sec->section}}" readonly>
                                <input type="hidden" name="year_section_id" value="{{$yr_sec->course_id}}">
                            </div>                        
                        @endforeach
                    </div>
                    <div class="basic_info section">
                        <div class="title_h4">
                            <h4>Student Information</h4>
                        </div>
                        <div class="last_name">
                            <div class="heading">
                                Last Name <span class="required">*</span>
                            </div>
                            <input type="text"  class="textbox"  placeholder="Last Name" name="last_name" value="{{old('last_name')}}" />
                        </div>
                        <div class="first_name">
                            <div class="heading">
                                First Name <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="First Name" name="first_name" value="{{old('first_name')}}"> 
                        </div>
                        <div class="middle_name">
                            <div class="heading">
                                Middle Name <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="Middle Name" name="middle_name" value="{{old('middle_name')}}">
                        </div>
                        <div class="suffix">
                            <div class="heading">
                                Suffix
                            </div>
                            <input type="text" placeholder="ex. Sr." name="suffix" value="{{old('suffix')}}"> 
                        </div>
                    </div>
                    <div class="basic_info_2 section">
                        <div class="birthday">
                            <div class="heading">
                                Birthday <span class="required">*</span>
                            </div>
                            <input type="date" name="birthday" value="{{old('birthday')}}">
                        </div>
                        <div class="sex">
                            <div class="heading">
                                Sex <span class="required">*</span>
                            </div>
                            <select name="gender" id="gender">
                                <option value="" disabled selected>Select Gender</option>
                                @foreach ($genders as $gender)
                                    <option value="{{$gender->id}}">{{$gender->gender}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="civil_status">
                            <div class="heading">
                                Civil Status <span class="required">*</span>
                            </div>
                            <select name="civil_status" id="civil_status">
                                <option value="" disabled selected>Select Civil Status</option>
                                @foreach ($civils as $civil)
                                    <option value="{{$civil->id}}">{{$civil->civil_status}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="basic_info_3 section">
                        <div class="nationality">
                            <div class="heading">
                                Nationality <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="ex. Filipino" name="nationality" value="{{old('nationality')}}"> 
                        </div>
                        <div class="religion">
                            <div class="heading">
                                Religion <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="ex. Roman Catholic" name="religion" value="{{old('religion')}}"> 
                        </div>
                    </div>
                    <div class="contact section">
                        <div class="title_h4">
                            <h4>Contact</h4>
                        </div>
                        <div class="email">
                            <div class="heading">
                                Email <span class="required">*</span>
                            </div>
                            <input type="text" id="email" placeholder="ex. xxxx@gmail.com" name="email" value="{{old('email')}}"> 
                            <p class="help-block error_message">{{$errors->first('email')}}</p>
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                            <span id="error_email"></span>
                        </div>
                        <div class="telephone">
                            <div class="heading">
                                Telephone #
                            </div>
                            <input type="text" placeholder="Telephone #" name="telephone_number" value="{{old('telephone_number')}}"> 
                        </div>
                        <div class="mobilephone">
                            <div class="heading">
                                Mobile # <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="ex. 09xxxxxxxx" name="mobile_number" value="{{old('mobile_number')}}"> 
                        </div>
                    </div>
                    <div class="address section">
                        <div class="title_h4">
                            <h4>Address</h4>
                        </div>
                        <div class="province">
                            <div class="heading">
                                Province <span class="required">*</span>
                            </div>
                            <select name="province_id" id="province">
                                <option value="0" disabled selected>Select Province</option>
                                @foreach ($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->province_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="municipality">
                            <div class="heading">
                                Municipality <span class="required">*</span>
                            </div>
                            <select name="city" id="city">
                                <option value="0" disabled selected>Select Municipality</option>
                            </select>
    
                        </div>
                        <div class="barangay">
                            <div class="heading">
                                Barangay <span class="required">*</span>
                            </div>
                            <select name="barangay" id="barangay">
                                <option value="0" disabled selected>Select Barangay</option>
                            </select> 
                        </div>
                        <div class="street">
                            <div class="heading">
                                Street <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="Street" name="street" value="{{old('street')}}"> 
                        </div>
                    </div>
                    <div class="guardian section">
                        <div class="title_h4">
                            <h4>Guardian</h4>
                        </div>
                        <div class="name">
                            <div class="heading">
                                Name <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="Full Name" name="guardian_name" value="{{old('guardian_name')}}"> 
                        </div>
                        <div class="mobilephone">
                            <div class="heading">
                                Contact # <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="ex. 09xxxxxxxx" name="guardian_contact" value="{{old('guardian_contact')}}"> 
                        </div>
                        <div class="guardian_address">
                            <div class="heading">
                                Address <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="Full Address" name="guardian_address" value="{{old('guardian_address')}}"> 
                        </div>
                        <div class="relationship">
                            <div class="heading">
                                Relationship <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="Relationship" name="guardian_relationship" value="{{old('guardian_relationship')}}"> 
                        </div>
                        <div class="occupation">
                            <div class="heading">
                                Occuopation <span class="required">*</span>
                            </div>
                            <input type="text" placeholder="Occupation" name="guardian_occupation" value="{{old('guardian_occupation')}}"> 
                        </div>
                    </div>
                    <div class="terms_condition section">
                        <div class="title_h4 title_term">
                            <h4>Terms and Conditon</h4>
                        </div>
                        <div class="paragraph">
                            <p id="term_condition" class="para">By clicking the check box, you <span class="term_condition">agree</span>  with our <span class="term_condition">terms</span> and <span class="term_condition">conditions</span>. The information provided by you in this application form will be used by the Bulacan Polytechnic College for student information management purposes only.</p> 
                        </div>
                        <div class="check">
                            <label for="agree" class="agree">Accept</label>
                            <input type="checkbox" name="checkbox" id="agree" name="agree">
                        </div>
                    </div>
                    <div class="captcha section">
                        <div class="re_captcha">
                            <div class="g-recaptcha" data-sitekey="{{env('CAPTCHA_KEY')}}" data-callback="verifyCapcha"></div>
                            <p class="help-block error_message">{{$errors->first('g-recaptcha-response')}}</p>
                            <div id="g-recaptcha-error"></div>
                        </div>
                        <div class="submit">
                            <button type="submit" class="btn submit-btn">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            </form> 
            <div class="footer">

            </div>
            {{-- @include('js.registration_registered') --}}
            <script>
                $("#form").validate({
             
             rules: {
                 email:{
                     required:true,
                     email:true,
                 },
                 checkbox:{
                     required: true,
                 },
                 last_name: {
                     required: true,
                     maxlength: 30
                 },
                 first_name: {
                     required: true,
                     maxlength: 30
                 },
                 middle_name: {
                     required: true,
                     maxlength: 30
                 },
                 suffix: {
                     maxlength: 10
                 },
                 gender: {
                     required: true,
                 },
                 birthday: {
                     required: true,
                 },
                 civil_status: {
                     required: true,
                 },
                 nationality: {
                     required: true,
                 },
                 religion: {
                     required: true,
                 },
                 telephone_number: {
                     digits: true,
                 },
                 mobile_number: {
                     required: true, digits: true, minlength: 11, maxlength: 11
                 },
                 barangay: {
                     required: true,
                 },
                 street: {
                     required: true, maxlength: 30,
                 },
                 city: {
                     required: true,
                 },
                 province_id: {
                     required: true,
                 },
                 guardian_name: {
                     required: true,
                 },
                 guardian_contact: {
                     required: true, digits: true
                 },
                 guardian_address: {
                     required: true,
                 },
                 guardian_relationship: {
                     required: true,
                 },
                 guardian_occupation: {
                     required: true,
                 },
                 
             },
             messages: {
                 checkbox:{
                     required:  "This field is reqiured!",
                 },
                 last_name: {
                     required: "This field is reqiured!",
                     maxlength: "This field should less than or equal to {0} characters.",
                 },
                 first_name: {
                     required: "This field is reqiured!",
                     maxlength: "This field should less than or equal to {0} characters.",
                 },
                 middle_name: {
                     required: "This field is reqiured!",
                     maxlength: "This field should less than or equal to {0} characters.",
                 },
                 suffix: {
                     maxlength: "This field should less than or equal to {0} characters.",
                 },
                 gender: {
                     required: "This field is reqiured!",
                 },
                 birthday: {
                     required: "This field is reqiured!",
                 },
                 civil_status: {
                     required: "This field is reqiured!",
                 },
                 nationality: {
                     required: "This field is reqiured!",
                 },
                 religion: {
                     required: "This field is reqiured!",
                 },
                 telephone_number: {
                     digits: 'Please enter numeric characters only.',
                 },
                 mobile_number: {
                     required: "This field is reqiured!", digits: 'Please enter numeric characters only.', minlength:'This field should be {0} characters.', maxlength: 'This field should {0} characters.'
                 },
                 barangay: {
                     required:  "This field is reqiured!",
                 },
                 street: {
                     required:  "This field is reqiured!", maxlength: 'This field  must be less than {0} characters.'
                 },
                 city: {
                     required:  "This field is reqiured!",
                 },
                 province_id: {
                     required:  "This field is reqiured!",
                 },
                 guardian_name: {
                     required:  "This field is reqiured!",
                 },
                 guardian_contact: {
                     required:  "This field is reqiured!", digits: 'Please enter numeric characters only.',
                 },
                 guardian_address: {
                     required:  "This field is reqiured!",
                 },
                 guardian_relationship: {
                     required: "This field is reqiured!",
                 },
                 guardian_occupation: {
                     required: "This field is reqiured!",
                 },
                 email: {
                     required: "This field is reqiured!",
                     email: "This email is invalid.",
                 },
             },
         })
            </script>
            <script>
                function submit_form(){

                    var response = grecaptcha.getResponse();
                    if(response.length == 0){
                        document.getElementById('g-recaptcha-error').innerHTML='<span class="error">This field is required!</span>'
                        return false;
                    }
                    return true;
                }
                function verifyCapcha(){
                    document.getElementById('g-recaptcha-error').innerHTML='';
                }

                
                jQuery(document).ready(function (){
                    jQuery('select[name="province_id"]').on('change',function(){
                        var provinceID = jQuery(this).val();
                        if(provinceID){
                            jQuery.ajax({
                            url : 'address/city/' +provinceID,
                            type : "GET",
                            dataType : "json",
                            success:function(data){
                                jQuery('select[name="city"]').empty();
                                jQuery('select[name="city"]').append('<option value="">'+ 'Select Municipality'+'</option>');
                                jQuery('select[name="barangay"]').empty();
                                jQuery('select[name="barangay"]').append('<option value="">'+ 'Select Barangay'+'</option>');
                                jQuery.each(data, function(key,value){
                                $('select[name="city"]').append('<option value="'+ key +'">'+ value +'</option>');
                                });
                            }
                            });
                        }
                        else{
                            $('select[name="city"]').empty();
                        }
                    });
                    jQuery('select[name="city"]').on('change',function(){
                        var cityID = jQuery(this).val();
                        if(cityID){
                        jQuery.ajax({
                            url : 'address/barangay/' +cityID,
                            type : "GET",
                            dataType : "json",
                            success:function(data){
                            jQuery('select[name="barangay"]').empty();
                            jQuery('select[name="barangay"]').append('<option value="">'+ 'Select Barangay'+'</option>');
                            

                            jQuery.each(data, function(key,value){
                                $('select[name="barangay"]').append('<option value="'+ key +'">'+ value +'</option>');
                            });
                            }
                        });
                        }
                        else{
                        $('select[name="barangay"]').empty();
                        }
                    });
                    $('#email').blur(function(){
                        var email = jQuery(this).val();
                        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        if(!filter.test(email)){    
                            // $('#error_email').html('<span class="error_email">This Email address is invalid!q</span>');
                            // $('#email').addClass('has-error');
                        }
                        else if(email ==""){
                            $('#error_email').html('<span class="success_email"></span>');
                                        $('#email').removeClass('has-error');
                        }else{
                        
                         
                        
                            jQuery.ajax({
                                url :'email/' +email,
                                type : "GET",
                                dataType : "json",
                                success: function(response) {
                                    if(response == 1){
                                        $('#error_email').html('<span class="error_email">This Email is not available!</span>');
                                        $('#email').addClass('has-error');
                                    }else{
                                        $('#error_email').html('<span class="success_email">This Email is available.</span>');
                                        $('#email').removeClass('has-error');
                                    }
                                }
                            });
                        }
                    });
                });

                

                    

             </script>
    </body>
</html>