
<?php  
 $connect = mysqli_connect("localhost", "wbbssi", "^5.nM~;akxFe", "wbbssi_login1");  
 $query = "SELECT municipality, count(*) as number FROM survey GROUP BY municipality";  
 $result = mysqli_query($connect, $query);  
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  
           <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>  
           <script type="text/javascript">  
           google.charts.load('current', {'packages':['corechart']});  
           google.charts.setOnLoadCallback(drawChart);  
           function drawChart()  
           {  
                var data = google.visualization.arrayToDataTable([  
                          ['municipality', 'Number'],  
                          <?php  
                          while($row = mysqli_fetch_array($result))  
                          {  
                               echo "['".$row["municipality"]."', ".$row["number"]."],";  
                          }  
                          ?>  
                     ]);  
                var options = {  
                 
                      //is3D:true,  
                      pieHole: 0.5 
                     };  
                var chart = new google.visualization.PieChart(document.getElementById('piechart'));  
                chart.draw(data, options);  
           }  
           </script>  
      </head>  
      <body>  
           <br /><br />  
           <div style="width:900px;">  
                <h3 align="center">'Percentage of Conducted Survey by Municipality'</h3>  
                <br />  
                <div id="piechart" style="width: 900px; height: 500px;"></div>  
           </div>  
      </body>  
 </html>  