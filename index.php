<html lang="en">

<body>

  <header><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
    
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
      <div class="container">
        <a class="navbar-brand" href="#"><strong>WBBSSI</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
          aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact Us</a>
            </li>
          </ul>
          <form class="form-inline">
            <div class="md-form my-0">

            </div>
          </form>
        </div>
      </div>
    </nav>
    <!-- Navbar -->
    <!-- Full Page Intro -->
    <div class="view"
      style="background-image: url('b.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
      <!-- Mask & flexbox options-->
      <div class="align-items-center">
        <!-- Content -->
        <div class="container">
          <!--Grid row-->
          <div class="row">
            <!--Grid column-->
            <div class="col-md-12 mb-4 white-text text-center table-center" id="nav">
              <br><br>
              <h1
                class="h1-reponsive display-4 white-text text-white font-weight-bold mb-0 pt-md-5 pt-25 wow fadeInDown"
                data-wow-delay="1.5s"><strong>Online Survey For Investor</strong></h1>
              <br><br><br>

              <a href="" class="btn btn-success text-center white-text" id="btn_signin" data-toggle="modal"
                data-target="#modalSignIn">Sign In</a>
              <a href="" class="btn btn-success text-center white-text" id="btn_register" data-toggle="modal"
                data-target="#modalRegister">Register</a>



            </div>
            <!--Grid column-->
          </div>
          <!--Grid row-->
        </div>
        <!-- Content -->
      </div>
      <!-- Mask & flexbox options-->
    </div>
    <!-- Full Page Intro -->
  </header>
  <!-- Main navigation -->
  <!--Main Layout-->
  <style type="text/css">
    html,
    body,
    header,
    .view {
      height: 100%;
    }

    @media (max-width: 740px) {

      html,
      body,
      header,
      .view {
        height: 100vh;
      }
    }

    .top-nav-collapse {
      background-color: blue !important;
    }

    .navbar:not(.top-nav-collapse) {
      background: transparent !important;
    }

    @media (max-width: 991px) {
      .navbar:not(.top-nav-collapse) {
        background: #78909c !important;
      }
    }

    h1 {
      letter-spacing: 8px;
    }

    h5 {
      letter-spacing: 3px;
    }

    .hr-light {
      border-top: 3px solid #fff;
      width: 80px;
    }

    #nav {
      padding-top: 150px;
    }

    #btn_signin {
      height: 50px;
      width: 150px;
    }

    #btn_register {
      height: 50px;
      width: 150px;
    }
  </style>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
  </script>




  <main>
    <div class="container">
      <!--Grid row-->
      <div class="row py-5">
        <!--Grid column-->
        <div class="col-md-12 text-center">

        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main Layout-->
  <script type="text/javascript">
    // Animations init
    new WOW().init();
  </script>
  <!-----------------------------------------------------modal Sign in----------------------------------------------------->



  <div class="modal fade" id="modalSignIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold">Sign In As</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-footer d-flex justify-content-center">
          <a href="" class="btn btn-success text-center white-text" id="" data-toggle="modal"
            data-target="#customerSignIn">Customer</a>
          <a href="" class="btn btn-success text-center white-text" id="" data-toggle="modal"
            data-target="#InvestorSignIn">Investor</a>
            <a href="" class="btn btn-success text-center white-text" id="" data-toggle="modal"
            data-target="#administratorSignIn">Adminstrator</a>
        </div>
      </div>
    </div>
  </div>


  <!----------------------------------------------------------------end----------------------------------------------------------->
  <!-----------------------------------------------------modal registration-------------------------------------------------------------->



  <div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold">Register As</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-footer d-flex justify-content-center">
          <a href="" class="btn btn-success text-center white-text" id="" data-toggle="modal"
            data-target="#CustomerRegister">Customer</a>
          <a href="" class="btn btn-success text-center white-text" id="" data-toggle="modal"
            data-target="#InvestorRegister">Investor</a>
        </div>
      </div>
    </div>
  </div>


  <!----------------------------------------------------------------end----------------------------------------------------------->
  <!----------------------------------------------------------------modal signin customer---------------------------------------------------------->

  <div class="modal fade" id="customerSignIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold">Customer SignIn</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="customer_login.php">
          <!-------------------------------------action---------------------------->
          <div class="modal-body mx-3">
            <div class="md-form mb-5">
              <input type="email" id="defaultForm-email" class="form-control validate" name="email" placeholder="Email"
                required>
            </div>

            <div class="md-form mb-4">
              <input type="password" id="defaultForm-pass" class="form-control validate" name="password"
                placeholder="Password" required>
            </div>

          </div>
          <div class="modal-footer d-flex justify-content-center">
            <input class="btn btn-success" type="submit" name="submit" value="login">
          </div>
        </form>
      </div>
    </div>
  </div>


  <!----------------------------------------------------------------end----------------------------------------------------------->
  <!----------------------------------------------------------------modal signin Administrator---------------------------------------------------------->

  <div class="modal fade" id="administratorSignIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold">Administator Signin</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="administrator_login.php">
          <!-------------------------------------action---------------------------->
          <div class="modal-body mx-3">
            <div class="md-form mb-5">
              <input type="email" id="defaultForm-email" class="form-control validate" name="email" placeholder="Email"
                required>
            </div>

            <div class="md-form mb-4">
              <input type="password" id="defaultForm-pass" class="form-control validate" name="password"
                placeholder="Password" required>
            </div>

          </div>
          <div class="modal-footer d-flex justify-content-center">
            <input class="btn btn-success" type="submit" name="submit" value="login">
          </div>
        </form>
      </div>
    </div>
  </div>


  <!----------------------------------------------------------------end----------------------------------------------------------->
  <!----------------------------------------------------------------modal signin Investor---------------------------------------------------------->

  <div class="modal fade" id="InvestorSignIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold">Investor SignIn</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="investor_sign.php">
          <div class="modal-body mx-3">
            <div class="md-form mb-5">
              <input type="email" id="defaultForm-email" class="form-control validate" name="email" placeholder="Email"
                required>
            </div>

            <div class="md-form mb-4">
              <input type="password" id="defaultForm-pass" class="form-control validate" name="password"
                placeholder="Password" required>
            </div>

          </div>
          <div class="modal-footer d-flex justify-content-center">
            <input class="btn btn-success" type="submit" name="submit" value="login">
          </div>
        </form>
      </div>
    </div>
  </div>


  <!----------------------------------------------------------------end----------------------------------------------------------->
  <!----------------------------------------------------------------modal customer SignUp---------------------------------------------------------->


  <div class="modal fade" id="CustomerRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold">Customer Registration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="sample_register_code.php" enctype="multipart/form-data">
          <div class="modal-body mx-3">
            <div class="md-form mb-9">
              <input type="text" id="defaultForm-email" class="form-control validate" name="firstname"
                placeholder="First Name" required>
            </div>
            <div class="md-form mb-8">
              <input type="text" id="defaultForm-pass" class="form-control validate" name="middlename"
                placeholder="Middle Name">
            </div>
            <div class="md-form mb-7">
              <input type="text" id="defaultForm-email" class="form-control validate" name="lastname"
                placeholder="Last Name">
            </div>
            <div class="md-form mb-6">
              <input type="date" id="defaultForm-pass" class="form-control validate" name="age" placeholder="Age"
                required>
            </div>

            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Gender</label>
              </div>
              <select class="custom-select" id="inputGroupSelect01" name="gender">
                <option selected>Choose...</option>
                <option value="male">male</option>
                <option value="female">female</option>
              </select>
            </div>
            <div class="md-form mb-4">
              <input type="number" id="defaultForm-pass" class="form-control validate" name="contact"
                placeholder="Contact Number" required>
            </div>

            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">municipality</label>
              </div>
              <select class="custom-select" id="inputGroupSelect01" name="municipality">
                <option>Select Municipality</option>
                <option value="1">Angat</option>
                <option value="2">Balagtas</option>
                <option value="3">Baliuag</option>
                <option value="4">Bocaue</option>
                <option value="5">Bulackan</option>
                <option value="6">Bustos</option>
                <option value="7">Calumpit</option>
                <option value="8">Doña Remedios Trinidad</option>
                <option value="9">Guiginto</option>
                <option value="10">Hagonoy</option>
                <option value="11">Malolos</option>
                <option value="12">Marilao</option>
                <option value="13">Meycauayan</option>
                <option value="14">Norzagaray</option>
                <option value="15">Obando</option>
                <option value="16">Pandi</option>
                <option value="17">Paombong</option>
                <option value="18">Plaridel</option>
                <option value="19">Pulilan</option>
                <option value="20">San Ildefonso</option>
                <option value="21">San Jose Del Monte</option>
                <option value="22">San Miguel</option>
              </select>
            </div>


            <div class="md-form mb-2">
              <input type="email" id="defaultForm-pass" class="form-control validate" name="email" placeholder="EX@.com"
                required>
            </div>
            <div class="md-form mb-1">
              <input type="password" id="defaultForm-pass" class="form-control validate" name="password"
                placeholder="Password" required>
            </div>
            <div class="md-form mb-1">
              <input type="file" name="picture" required>
            </div>

          </div>
          <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-success" type="submit" name="submit">SignUp</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!----------------------------------------------------------------end----------------------------------------------------------->
  <!----------------------------------------------------------------modal Investor SignUp---------------------------------------------------------->

  <div class="modal fade" id="InvestorRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold">Investor Registration</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="investor_register.php" enctype="multipart/form-data">
          <div class="modal-body mx-3">
            <div class="md-form mb-9">
              <input type="text" id="defaultForm-email" class="form-control validate" name="firstname"
                placeholder="First Name" required>
            </div>
            <div class="md-form mb-8">
              <input type="text" id="defaultForm-pass" class="form-control validate" name="middlename"
                placeholder="Middle Name" required>
            </div>
            <div class="md-form mb-7">
              <input type="text" id="defaultForm-email" class="form-control validate" name="lastname"
                placeholder="Last Name">
            </div>
            <div class="md-form mb-6">
              <input type="date" id="defaultForm-pass" class="form-control validate" name="age" placeholder="Age"
                required>
            </div>


            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Gender</label>
              </div>
              <select class="custom-select" id="inputGroupSelect01" name="gender">
                <option selected>Select Gender</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
              </select>
            </div>
            <div class="md-form mb-4">
              <input type="number" id="defaultForm-pass" name="contact" class="form-control validate" placeholder="Contact Number"
                required>
            </div>

            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">municipality</label>
              </div>
              <select class="custom-select" id="inputGroupSelect02" name="municipality">
                <option>Select Municipality</option>
                <option value="1">Angat</option>
                <option value="2">Balagtas</option>
                <option value="3">Baliuag</option>
                <option value="4">Bocaue</option>
                <option value="5">Bulackan</option>
                <option value="6">Bustos</option>
                <option value="7">Calumpit</option>
                <option value="8">Doña Remedios Trinidad</option>
                <option value="9">Guiginto</option>
                <option value="10">Hagonoy</option>
                <option value="11">Malolos</option>
                <option value="12">Marilao</option>
                <option value="13">Meycauayan</option>
                <option value="14">Norzagaray</option>
                <option value="15">Obando</option>
                <option value="16">Pandi</option>
                <option value="17">Paombong</option>
                <option value="18">Plaridel</option>
                <option value="19">Pulilan</option>
                <option value="20">San Ildefonso</option>
                <option value="21">San Jose Del Monte</option>
                <option value="22">San Miguel</option>
              </select>
              <br>
            </div>


            <div class="md-form mb-2">
              <input type="email" id="defaultForm-pass" class="form-control validate" name="email" placeholder="EX@.com"
                required>
            </div>
            <div class="md-form mb-1">
              <input type="password" id="defaultForm-pass" class="form-control validate" name="password"
                placeholder="Password" required>
            </div>
            <div class="md-form mb-1">
              <input type="file" name="picture" required>
            </div>

          </div>
          <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-success">SignUp</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <!----------------------------------------------------------------end----------------------------------------------------------->

</body>


</html>