<?php
include('emailbookduedate.ph');
include('main_connection.php');

//include("functions.php");

?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="admin.php">Online Survey For Investor</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="radmin.php">Register<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="add.php">Add Trivia</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="add.php">Update/Delete</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Statistics
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index4.php">To Shop</a>
          <a class="dropdown-item" href="index5.php">To Watch</a>
          <a class="dropdown-item" href="index6.php">To Eat</a>
          <a class="dropdown-item" href="index7.php">To Have Fun</a>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pending
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="table.php">Investor Request</a>
          <a class="dropdown-item" href="table.php">Survey Request</a>
      </li>
      <li><a href="logout.php"  onclick="return confirm('Are you sure you want to Log-out')">Logout</a></li> 	 
    </ul>
    
  </div>
</nav>
<br>
        <div class="card-body margin-top">
          <h5 class="card-title">Investor Request</h5>

<table class="table table-light table-hover table-bordered table-responsive" id="myTable">
  <thead>
    <tr>
      <th class="text-center">First Name:</th>
      <th class="text-center">Middle Name</th>
      <th class="text-center">Last Name</th>
      <th class="text-center">Gender</th>
      <th class="text-center">Age</th>
      <th class="text-center">Email Address</th>
      <th class="text-center">Contact</th>
      <th class="text-center">Action</th>
    </tr>
  </thead>
      <?php
     
$query = "SELECT * FROM `investor_pending` order by id DESC";
$result = mysqli_query($conn,$query);

      while($row = mysqli_fetch_array($result))
      {
        echo '
            <tr>
                <td class="text-center">'.$row["firstname"].'</td>
                <td class="text-center">'.$row["middlename"].'</td>
                <td class="text-center">'.$row["lastname"].'</td>
                <td class="text-center">'.$row["gender"].'</td>
                <td class="text-center">'.$row["age"].'</td>
                <td class="text-center">'.$row["email"].'</td>
                <td class="text-center">'.$row["contact"].'</td>
                
                
   <td class="text-center"> <a href="reject.php?id='.$row["id"].'"  class="btn btn-danger badge-pill id="Reject">Reject</a>
   
    <a href="pending.php?id='.$row["id"].'" class="btn btn-success badge-pill id="Accept">Accept</a>
    </td>

        ';
        
        }
      ?>

         
     
</table>

          
        </div>
      </div>

      <div>
      <h5 class="card-title">Approved Investors</h5>

<table class="table table-light table-hover table-bordered table-responsive" id="myTable">
  <thead>
    <tr>
      <th class="text-center">First Name:</th>
      <th class="text-center">Middle Name</th>
      <th class="text-center">Last Name</th>
      <th class="text-center">Gender</th>
      <th class="text-center">Age</th>
      <th class="text-center">Email Address</th>
      <th class="text-center">Contact</th>
    </tr>
  </thead>
      <?php
     
      $query = "SELECT * FROM `investor_pending` where `status` = '1' order by id DESC";
      $result = mysqli_query($conn,$query);
      while($row = mysqli_fetch_array($result))
      {
        echo '
            <tr>
                <td class="text-center">'.$row["firstname"].'</td>
                <td class="text-center">'.$row["middlename"].'</td>
                <td class="text-center">'.$row["lastname"].'</td>
                <td class="text-center">'.$row["gender"].'</td>
                <td class="text-center">'.$row["age"].'</td>
                <td class="text-center">'.$row["email"].'</td>
                <td class="text-center">'.$row["contact"].'</td>
                
                


        ';
        
        }
      ?>

         
     
</table>
      </div>

      
      <div>
      <h5 class="card-title">Rejected Investors</h5>

<table class="table table-light table-hover table-bordered table-responsive" id="myTable">
  <thead>
    <tr>
      <th class="text-center">First Name:</th>
      <th class="text-center">Middle Name</th>
      <th class="text-center">Last Name</th>
      <th class="text-center">Gender</th>
      <th class="text-center">Age</th>
      <th class="text-center">Email Address</th>
      <th class="text-center">Contact</th>
    </tr>
  </thead>
      <?php
     
      $query = "SELECT * FROM `investor_pending` where `status` = 'REJECTED' order by id DESC";
      $result = mysqli_query($conn,$query);
      while($row = mysqli_fetch_array($result))
      {
        echo '
            <tr>
                <td class="text-center">'.$row["firstname"].'</td>
                <td class="text-center">'.$row["middlename"].'</td>
                <td class="text-center">'.$row["lastname"].'</td>
                <td class="text-center">'.$row["gender"].'</td>
                <td class="text-center">'.$row["age"].'</td>
                <td class="text-center">'.$row["email"].'</td>
                <td class="text-center">'.$row["contact"].'</td>
                
                


        ';
        
        }
      ?>

         
     
</table>
      </div>

      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript">
    $(document).ready( function () {
      $('#myTable').DataTable();
} );
  </script>
  <script type="text/javascript">
    $("#Accept").click(function(){

      swal("Good job!", "You clicked the button!", "success");
      
    });
  </script>

  </body>

</html>