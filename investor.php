<?php
include('main_connection.php');
// include('emailbookduedate.php');
$email = $_SESSION['login_user']; 
//echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';

$result = mysqli_query($conn,"SELECT firstname,middlename,lastname,age,gender,contact,municipality,email,picture FROM investor_pending WHERE email='$email'");
$retrive = mysqli_fetch_array($result);

$firstname = $retrive['firstname'];
$middlename = $retrive['middlename'];
$lastname = $retrive['lastname'];
$age = $retrive['age'];
$gender = $retrive['gender'];
$contact = $retrive['contact'];
$municipality = $retrive['municipality'];
$email = $retrive['email'];
$picture = $retrive['picture'];

?>




<!doctype html>
<html lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Investor Profile</title>
  </head>

  <style>
    #img{
        width: 100px;
        height: 100px;
    }

</style>
  <body>
   

<div class="container">
    <div class="row my-2">
        <div class="col-lg-8 order-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#Trivia" data-toggle="tab" class="nav-link">Trivia</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Request Survey</a>
                </li>
            </ul>
            <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                    <h5 class="mb-3">Investor Profile</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>Full Name</h6>
                            <p>
                                <?php echo $firstname."&nbsp".$middlename."&nbsp".$lastname ?>
                            </p>
                            <h6>age</h6>
                            <p>
                                <?php echo $age ?>
                            </p>
                            <h6>Gender</h6>
                            <p>
                                <?php echo $gender ?>
                            </p>
                            <h6>Municipality</h6>
                            <p>
                              <?php echo $municipality ?>
                            </p>
                             <h6>Contact</h6>
                            <p>
                                <?php echo $contact ?>
                            </p>
                            <h6>Email</h6>
                            <p>
                                <?php echo $email ?>
                            </p>
                            


                        </div>
                       
                        
                    </div>
                    <!--/row-->
                </div>

                <div class="tab-pane" id="messages">
                    <div class="alert alert-info alert-dismissable">
                        <a class="panel-close close" data-dismiss="alert">×</a> This is an <strong>.alert</strong>. Use this to show important messages to the user.
                    </div>
                    <table class="table table-hover table-striped">
                        <tbody>                                    
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">3 hrs ago</span> Here is your a link to the latest summary report from the..
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">Yesterday</span> There has been a request on your account since that was..
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">9/10</span> Porttitor vitae ultrices quis, dapibus id dolor. Morbi venenatis lacinia rhoncus. 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">9/4</span> Vestibulum tincidunt ullamcorper eros eget luctus. 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">9/4</span> Maxamillion ais the fix for tibulum tincidunt ullamcorper eros. 
                                </td>
                            </tr>
                        </tbody> 
                    </table>
                </div>
                <div class="tab-pane" id="edit">
                    <form role="form">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Full name</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" value="<?php echo $firstname." ".$middlename." ".$lastname ?>"disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Age</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" value="<?php echo $age ?>"disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Contact</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="email" value="<?php echo $contact ?>"disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Email</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" value="<?php echo $email ?>"disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9">
                                <input type="button" class="btn btn-primary" value="Submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-4 order-lg-1 text-center">
            <img src='images/<?php echo $picture ?>' class="mx-auto img-fluid img-circle d-block" alt="avatar" id="img">
            <h6 class="mt-2">Upload a different photo</h6>
            <label class="custom-file">
                <input type="file" id="file" class="custom-file-input">
                <a href="logout_user.php"><input type="submit"class="btn btn-danger"name="submit"value="Log Out"></a>
            </label>
        </div>
    </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>





